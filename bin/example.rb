#!/usr/bin/ruby
require_relative '../lib/fizz_buzzer'

nada = Condition.new(
  priority: 1,
  action: proc { |x| puts x },
  conditional: proc { true }
)

fizz = Condition.new(
  priority: 2,
  action: proc { puts 'Fizz' },
  conditional: proc { |x| x.modulo(3).zero? }
)

buzz = Condition.new(
  priority: 2,
  action: proc { puts 'Buzz' },
  conditional: proc { |x| x.modulo(5).zero? }
)

fizzbuzz = Condition.new(
  priority: 3,
  action: proc { puts 'Fizzbuzz' },
  conditional: proc { |x| x.modulo(15).zero? }
)

palindrome = Condition.new(
  action: proc { puts 'TACOCAT' },
  conditional: proc { |x| x.to_s.length > 1 && x.to_s.reverse == x.to_s }
)

fizzbuzzer = FizzBuzzer.new(
  ConditionGroup.new(nada, fizz, buzz, fizzbuzz)
)

(1..15).each { |num| fizzbuzzer.eval(num) }

puts
fizzbuzzer.add(palindrome)

(1..15).each { |num| fizzbuzzer.eval(num) }
