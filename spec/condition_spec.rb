require 'spec_helper'

RSpec.describe Condition, '#initialize' do
  context 'with no arguments' do
    it 'can create a working condition' do
      condition = Condition.new
      condition.run(1)
      condition.eval(1)
    end
  end
end

RSpec.describe Condition, '#run' do
  input  = 1
  output = 'I worked'

  context 'when asked to run' do
    it 'runs the action' do
      condition = Condition.new(action: proc { print output })
      expect { condition.run(input) }.to output(output).to_stdout
    end
  end
end

RSpec.describe Condition, '#eval' do
  input  = 1
  output = 'I worked'

  context 'with an input type that can\'t run the conditional' do
    it 'returns false' do
      condition = Condition.new(
        conditional: proc { |x| x.method_that_doesnt_exist_for_type }
      )

      expect(condition.eval(input)).to eq false
    end
  end

  context 'with a true conditional' do
    it 'runs the action' do
      condition = Condition.new(
        conditional: proc { true },
        action: proc { print output }
      )

      expect { condition.run(input) if condition.eval(input) }.to output(output).to_stdout
    end
  end

  context 'with an false conditional' do
    it 'doesn\'t run the action' do
      condition = Condition.new(
        conditional: proc { false },
        action: proc { print output }
      )

      expect { condition.run(input) if condition.eval(input) }.to output('').to_stdout
    end
  end
end
