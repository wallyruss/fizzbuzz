require 'spec_helper'

RSpec.describe ConditionGroup, '#initialize' do
  context 'with no input' do
    it 'can initialize' do
      ConditionGroup.new
    end
  end

  context 'with conditions as input' do
    it 'can initialize' do
      ConditionGroup.new(Condition.new, Condition.new)
    end
  end

  context 'with an array of conditions as input' do
    it 'can initialize' do
      ConditionGroup.new([Condition.new, Condition.new])
    end
  end

  context 'with input that doesn\'t implement #eval' do
    it 'raises an ArgumentError' do
      expect { ConditionGroup.new('asdf') }.to raise_error(ArgumentError)
    end
  end
end

RSpec.describe ConditionGroup, '#eval' do
  input  = 1
  context 'with no conditions set' do
    it 'returns false' do
      group = ConditionGroup.new
      expect(group.eval(input)).to eq false
    end
  end

  context 'with conditions set' do
    it 'can select multiple true conditions with the same priority' do
      condition1 = Condition.new(
        priority: 1,
        conditional: proc { true }
      )
      condition2 = Condition.new(
        priority: 1,
        conditional: proc { true }
      )

      conditions = [condition1, condition2]
      condition_group = ConditionGroup.new(conditions)
      condition_group.eval(input)

      expect(condition_group.chosen_conditions).to eq conditions
    end

    it 'selects the conditions with the highest priority' do
      condition1 = Condition.new(
        priority: 1,
        conditional: proc { true }
      )
      condition2 = Condition.new(
        priority: 2,
        conditional: proc { true }
      )
      condition3 = Condition.new(
        priority: 3,
        conditional: proc { false }
      )

      conditions = [condition1, condition2, condition3]
      condition_group = ConditionGroup.new(conditions)
      condition_group.eval(input)

      expect(condition_group.chosen_conditions).to eq [condition2]
    end

    it 'continues evaluation if the highest priority condition has the :continue flag' do
      condition1 = Condition.new(
        priority: 1,
        conditional: proc { true }
      )
      condition2 = Condition.new(
        priority: 2,
        conditional: proc { true }
      )
      condition3 = Condition.new(
        priority: 3,
        continue: true,
        conditional: proc { true }
      )

      conditions = [condition1, condition2, condition3]
      condition_group = ConditionGroup.new(conditions)
      condition_group.eval(input)

      expect(condition_group.chosen_conditions).to eq [condition3, condition2]
    end
  end
end

RSpec.describe ConditionGroup, '#run' do
  input   = 1
  output1 = 'I worked'
  output2 = 'I also worked'
  output3 = 'I didn\'t work'

  context 'with valid input' do
    it 'runs the condition\'s action after a successful eval' do
      condition = Condition.new(
        priority: 1,
        conditional: proc { true },
        action:  proc { print output1 }
      )

      condition_group = ConditionGroup.new(condition)
      condition_group.eval(input)
      expect { condition_group.run(input) }.to output(output1).to_stdout
    end

    it 'doesn\'t require #eval to be called first to work' do
      condition = Condition.new(
        priority: 1,
        conditional: proc { true },
        action:  proc { print output1 }
      )

      condition_group = ConditionGroup.new(condition)
      expect { condition_group.run(input) }.to output(output1).to_stdout
    end
  end

  context 'with equal priority conditions' do
    it 'runs all actions' do
      condition1 = Condition.new(
        priority: 1,
        conditional: proc { true },
        action:  proc { print output1 }
      )
      condition2 = Condition.new(
        priority: 1,
        conditional: proc { true },
        action:  proc { print output2 }
      )

      conditions = [condition1, condition2]
      condition_group = ConditionGroup.new(conditions)

      expect { condition_group.run(input) }.to output(output1 + output2).to_stdout
    end
  end

  context 'with conditions with priority discrepancies' do
    it 'only runs the first true highest priority action' do
      condition1 = Condition.new(
        priority: 1,
        conditional: proc { true },
        action:  proc { print output1 }
      )
      condition2 = Condition.new(
        priority: 2,
        conditional: proc { true },
        action:  proc { print output2 }
      )
      condition3 = Condition.new(
        priority: 3,
        conditional: proc { false },
        action:  proc { print output3 }
      )

      conditions = [condition1, condition2, condition3]
      condition_group = ConditionGroup.new(conditions)

      expect { condition_group.run(input) }.to output(output2).to_stdout
    end
  end

  context 'with a higher priority condition with the :continue flag set' do
    it 'runs a true lower priority condition' do
      condition1 = Condition.new(
        priority: 1,
        conditional: proc { true },
        action:  proc { print output1 }
      )
      condition2 = Condition.new(
        priority: 2,
        continue: true,
        conditional: proc { true },
        action:  proc { print output2 }
      )

      conditions = [condition1, condition2]
      condition_group = ConditionGroup.new(conditions)

      expect { condition_group.run(input) }.to output(output2 + output1).to_stdout
    end
  end
end
