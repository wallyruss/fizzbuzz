require 'spec_helper'

RSpec.describe FizzBuzzer, '#initialize' do
  context 'with no input' do
    it 'can initialize' do
      FizzBuzzer.new
    end
  end

  context 'with duplicate input' do
    it 'can dedupe input conditions' do
      condition = Condition.new
      buzzer = FizzBuzzer.new(condition, condition)

      expect(buzzer.conditions).to eq [condition]
    end
  end

  context 'with conditions and condition groups as input' do
    it 'can initialize' do
      condition1 = Condition.new
      condition2 = Condition.new
      condition3 = Condition.new

      FizzBuzzer.new(
        ConditionGroup.new(condition1, condition2),
        condition3
      )
    end
  end
end

RSpec.describe FizzBuzzer, '#add' do
  condition = Condition.new
  context 'with valid input' do
    it 'can add a condition' do
      buzzer = FizzBuzzer.new

      buzzer.add(condition)
      expect(buzzer.conditions).to eq [condition]
    end

    it 'can add multiple conditions' do
      buzzer     = FizzBuzzer.new
      condition2 = Condition.new
      condition3 = Condition.new
      condition4 = Condition.new
      conditions = [condition, condition2]

      buzzer.add(conditions)
      expect(buzzer.conditions).to eq conditions

      buzzer.add(condition3, condition4)
      expect(buzzer.conditions).to eq conditions + [condition3, condition4]
    end

    it 'removes duplicate conditions' do
      buzzer     = FizzBuzzer.new
      conditions = [condition, condition]

      buzzer.add(conditions)
      expect(buzzer.conditions).to eq [condition]
    end
  end
end

RSpec.describe FizzBuzzer, '#remove' do
  condition = Condition.new

  context 'with valid input' do
    it 'can remove the correct condition' do
      conditions = []
      buzzer     = FizzBuzzer.new

      (1..5).each do
        condition = Condition.new
        conditions << condition
        buzzer.add(condition)
      end

      (1..5).each do
        buzzer.remove(conditions.pop)
        expect(buzzer.conditions).to eq conditions
      end
    end
  end
end

RSpec.describe FizzBuzzer, '#eval' do
  input  = 1
  output = 'I worked'

  context 'with valid input' do
    it 'runs the action of a true conditional' do
      buzzer = FizzBuzzer.new
      condition = Condition.new(
        conditional: proc { true },
        action: proc { print output }
      )

      buzzer.add(condition)
      expect { buzzer.eval(input) }.to output(output).to_stdout
    end

    it 'doesn\'t run the action of a false conditional' do
      buzzer = FizzBuzzer.new
      condition = Condition.new(
        conditional: proc { false },
        action: proc { print output }
      )

      buzzer.add(condition)
      expect { buzzer.eval(input) }.to output('').to_stdout
    end

    it 'will run the action of true conditions regardless of priority differences' do
      buzzer = FizzBuzzer.new
      condition1 = Condition.new(
        priority: 2,
        conditional: proc { true },
        action: proc { print output }
      )
      condition2 = Condition.new(
        priority: 1,
        conditional: proc { true },
        action: proc { print output }
      )

      buzzer.add(condition1, condition2)
      expect { buzzer.eval(input) }.to output(output + output).to_stdout
    end
  end
end
