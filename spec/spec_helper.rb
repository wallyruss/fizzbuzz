require 'simplecov'

SimpleCov.start
SimpleCov.add_filter('spec/')

require_relative '../lib/fizz_buzzer.rb'
require_relative '../lib/condition.rb'
require_relative '../lib/condition_group.rb'
