require_relative 'condition'
require_relative 'condition_group'

class FizzBuzzer
  attr_reader :conditions

  def initialize(*conditions)
    @conditions = (conditions || []).uniq
  end

  def eval(input)
    conditions.each do |condition|
      condition.run(input) if condition.eval(input)
    end
  end

  def add(*args)
    @conditions += args.flatten
    conditions.uniq!
  end

  def remove(condition)
    conditions.delete(condition)
  end

  alias << add
  alias push add
end
