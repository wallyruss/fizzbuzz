class Condition
  attr_reader :action
  attr_reader :priority
  attr_reader :continue
  attr_reader :conditional

  def initialize(params = {})
    @priority    = params.fetch(:priority, 1)
    @action      = params.fetch(:action, proc {})
    @continue    = params.fetch(:continue, false)
    @conditional = params.fetch(:conditional, proc { false })
  end

  def eval(args)
    conditional.call(args)
  rescue NoMethodError
    false
  end

  def run(args)
    action.call(args)
  end
end
