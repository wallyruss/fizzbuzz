class ConditionGroup
  attr_reader :chosen_conditions

  def initialize(*conditions)
    @conditions = (conditions || []).flatten
    @conditions.each { |condition| raise ArgumentError unless condition.respond_to?(:eval) }
    @chosen_conditions = []
  end

  def eval(input)
    @chosen_conditions = []
    self.primed_for_run = true

    stop_eval = false
    conditions.group_by(&:priority).reverse_each do |_priority, same|
      same.each do |condition|
        if condition.eval(input)
          @chosen_conditions << condition
          stop_eval = true unless condition.continue
        end
      end
      break if stop_eval
    end

    stop_eval
  end

  def run(args)
    self.eval(args) unless primed_for_run
    @chosen_conditions.each { |condition| condition.run(args) }
    self.primed_for_run = false
  end

  private

  attr_reader   :conditions
  attr_accessor :primed_for_run
end
